import dash
from dash import html
from dash import dcc
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd
from figure_02 import figure_02

MAIN_FIGURE = figure_02()

app = dash.Dash(__name__)
app.layout = html.Div(children=[
    html.H1('Hello Dash!', style={
        'color': 'gray',
        'textAlign': 'center'
    }),
    html.Div(children='This web application is under construction ;)'),
    dcc.Graph(
        id='main_figure',
        figure=MAIN_FIGURE
    )
])


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', debug=True)
