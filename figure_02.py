from pandas.core.arrays.categorical import contains
import plotly.graph_objects as go
import plotly.express as px
import circlify as circ
import pandas as pd
from etl import get_year

# size of the outer circle, depends on the layout size
MAX_CIRCLE = 520
axes_props = dict(dtick=1,
                  range=[-1, 1],
                  constrain='domain')

# must be sorted from largest to smallest list
data = [21, 17, 13, 11, 7, 5, 3, 2, 1]

# circle memo: 'circle', 'exex={'datum': data[n]}', 'level', 'r', 'x', 'y'
circles = circ.circlify(data, show_enclosure=True)

# circles = [dict(x=c.x, y=c.y, r=c.r, level=c.level, ex=c.ex) for c in circles]
# df = pd.DataFrame(circles)

# df = get_year('2012')
# fig = go.Figure(data=[go.Scatter(
#     x=[c.x for c in circles], y=[c.y for c in circles],
#     mode='markers',
#     marker_size=[c.r for c in circles])
# ])

x = [c.x for c in circles]
y = [c.y for c in circles]
size = [c.r*MAX_CIRCLE for c in circles]
labels = [c.ex for c in circles]


def figure_02():
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=x,
        y=y,
        mode='markers',
        marker_size=size,
        text=[f'number: {i}' for i in range(10)],
        hovertemplate='<b>Prime </b><i>%{text}</i><extra></extra>',
        # hovertext=[f'text: {i}' for i in range(10)],

    ))
    fig.update_layout(
        width=700,
        height=700,
        # title="circle packing",
    )
    fig.update_yaxes(
        axes_props,
        scaleanchor="x",
        scaleratio=1,
    )
    fig.update_xaxes(
        axes_props
    )

    return fig
