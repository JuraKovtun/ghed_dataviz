import pandas as pd

file = 'data/ghed_snippet.xlsx'
df = pd.read_excel(file)


def get_year(year: str) -> pd.DataFrame:
    """retrieve data for a specified year
    use country name as index, indicators as columns
    """
    if year in df.columns:
        output = df[['Countries', 'Indicators', year]
                    ].iloc[1:].iloc[lambda x: x.index % 4 != 1]
        output = output.pivot(
            index='Countries', columns='Indicators', values=year)
        return output
    else:
        raise ValueError('No data available for year:', year)


if __name__ == '__main__':
    foo = get_year('2012')
    bar = foo.itertuples()
    for i, e in enumerate(bar):
        if i == 2:
            break
        print(*e)

    # for row in foo.itertuples():
    #     c, *e = list(row)
    #     print(c, e)
