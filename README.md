# Tools
Plotly, Dash, pandas

# To do
transform returned from circlify to pd.DataFrame
supply to px.scatter

write a function to circlify values
calculate circle parameters
return new df

# pipeline
get data for selelcted year
calculate circlify values

# GHED_dataviz

Global Health Expenditure data visualization
Refer WHO open data https://apps.who.int/nha/database

## Getting started
*Create circle packing visualisation of [GHED](https://apps.who.int/nha/database) data snippet*
prepare data with pandas
use circle packing library: https://pypi.org/project/circlify/
plot with plotly
publish by Dash
x-axis is years, color is type of spendings

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:268ad0449c0ac0813a0c252c380c22f0?https://www.makeareadme.com/) for this template.