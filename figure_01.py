import plotly.graph_objects as go


def figure_01():
    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=[0],
        y=[0],
        mode='markers',
        marker_size=[320]
    ))
    fig.update_layout(
        width=500,
        height=500,
        title="here comes the sun"
    )
    fig.update_yaxes(
        scaleanchor="x",
        scaleratio=1,
        dtick=1
    )
    fig.update_xaxes(
        range=[-1, 1],
        constrain='domain',
        dtick=1
    )

    return fig
